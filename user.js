class User {
	constructor(id, fname, lname, email) {
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
	}
	toString() {
		return `${this.id} ${this.fname} ${this.lname} ${this.email}`;
	}
}

module.exports = User