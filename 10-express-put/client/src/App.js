import React, {useState,useEffect} from 'react';
import './App.css';
import axios from 'axios';

function App() {
  const [users, setUsers] = useState([])

  useEffect(() => {
    const getData = async() => {
      let response = await axios.get('http://localhost:5000/cinema')
      let users = await response.data
      setUsers(users)
    }
    getData()
  },[])

  const postPebbles = () => {
    console.log("Inside postPebbles");
    const pebbles = {
      "_id": 1004,
      "first_name": "Pebbles",
      "last_name": "Flintstone",
      "email": "pebbles@gmail.com"
    }
    axios.post('http://localhost:5000/cinema/add', pebbles); 
  }

  const updateFred = () => {
    const fred = {
      "_id": 1001,
      "first_name": "Fred",
      "last_name": "Flintstone",
      "email": "fredflintstone@gmail.com"
    }
    axios.put('http://localhost:5000/cinema/1001', fred); 
    console.log('updateFred')
  }
  const postFail = () => {
    const fred = {
      "_id": 1,
      "first_name": "Fred",
      "last_name": "Flintstone",
      "email": "fred@gmail.com"
    }
    axios.post('http://localhost:5000/users/1', fred).catch(error => {
      alert("We have a problem")
    }); 
    console.log('post fail')
  }
  return(
    <ul>
      {users.map(user => <li key={user._id}>{user._id}--{user.first_name}--{user.last_name}--{user.email}</li>)}
      <button onClick={postPebbles}>Add Pebbles</button>
      <button onClick={updateFred}>Update Fred</button>
      <button onClick={postFail}>Post Fail Example</button>
    </ul>
  )
}

export default App;
