var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/users',{useNewUrlParser: true}, error => {
	error ? console.log('Connection failed: ${error}') :
	console.log('Successfully connected to MongoDB');
});
	
var userSchema = new mongoose.Schema( 
    {
      _id:        { type: String, required: true }, 
      first_name: { type: String, required: true },
      last_name:  { type: String, required: true },
      email:      { type: String, required: true }
    }
);

var User = mongoose.model('User', userSchema);

User.find({first_name: 'Fred'},function(error,user) {
	console.log(user);
});

var query = User.findOne({'_id': '1001'});

var result = []
query.select('first_name last_name email');
query.exec(function (err,user) {
	if (err) 
		return handleError(err);
	else {
		console.log('%s %s %s.', user.first_name, user.last_name, user.email);
		result = [user.first_name, user.last_name, user.email];
	}
});

const express = require('express');
let app = express();

app.get('/', (req,res) => {   
    res.send('Mongoose finds: '+result);  
});

const server = app.listen(3000, () => {  
    const SERVERHOST = server.address().address;      
    const SERVERPORT = server.address().port;  
    console.log('App listening at http://${SERVERHOST}:${SERVERPORT}');
});
