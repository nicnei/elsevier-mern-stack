var http=require('http')
const PORT = 6000;
const HOST = 'localhost'

var server=http.createServer((request,response) => {
	response.writeHead(200,{'Content-Type' : 'text/plain'});
	response.end('Hello Elsevier!\n');
	console.log('Server at http://${HOST}/${PORT}');
});
server.listen(PORT, HOST);