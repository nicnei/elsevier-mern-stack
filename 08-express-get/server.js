var mongoose = require('mongoose');
var express = require("express");
var app = express();
var router = express.Router();

mongoose.connect('mongodb://localhost:27017/users',{useNewUrlParser: true}, error => {
	error ? console.log('Connection failed: ${error}') :
	console.log('Successfully connected to MongoDB');
});

var userSchema = new mongoose.Schema( 
    {
      _id:        { type: String, required: true }, 
      first_name: { type: String, required: true },
      last_name:  { type: String, required: true },
      email:      { type: String, required: true }
    }
);
var User = mongoose.model('User', userSchema);
app.use("/cinema",router);

router.route('/').get((req,resp)=>{
	var query = User.find({}).then(users => {
		if (!users) {
			resp.json({message: 'No users'});
			return;
		} else {
			resp.json({users});
			return;
		}
	});
});

router.route('/:id').get((req,resp)=>{
	const id = req.params.id;
	var query = User.findOne({'_id': id}).then(user => {
		if (!user) {
			resp.json({message: 'User not found'});
			return;
		} else {
			resp.json({user});
			return;
		}
	});
});

app.listen(3000,()=>{
  console.log("Live at Port 3000");
});