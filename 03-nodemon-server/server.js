const express = require('express');
let app = express();

app.get('/', (req,res) => {   
    res.send('Hello Philadelphia');  
});

const server = app.listen(3000, () => {  
    const SERVERHOST = server.address().address;      
    const SERVERPORT = server.address().port;  
    console.log(`Example app listening at http://${SERVERHOST}:${SERVERPORT}`);
});
