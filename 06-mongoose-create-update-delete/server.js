var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/users',{useNewUrlParser: true}, error => {
	error ? console.log('Connection failed: ${error}') :
	console.log('Successfully connected to MongoDB');
});
	
var userSchema = new mongoose.Schema( 
    {
      _id:        { type: String, required: true }, 
      first_name: { type: String, required: true },
      last_name:  { type: String, required: true },
      email:      { type: String, required: true }
    }
);

//Inserting new document
var newUser = null;
var User = mongoose.model('User', userSchema);
User.create({
	'_id': '1004',
	'first_name': 'Pebbles',
	'last_name': 'Flintstone',
	'email': 'pebbles@gmail.com'
}, (err, result) => {
  if (err) console.log('Failed to create Pebbles: '+err);
  else {
	  console.log('Pebbles created: '+result);
	  newUser = result;
  }
});

//Deleteting document
User.deleteOne({first_name: 'Fred' }, (err) => {
  if (err) { console.log('Delete failed: '+err);}
  else console.log('Fred removed');
});

//Updating document
User.updateOne({
      "_id": "1002",
      "first_name": "Wilma",
      "last_name": "Flintstone",
      "email": "wilmaflintsone@gmail.com"
    }, (error, result) => {
		if (error) { console.log('Update failed: '+err);}
		else console.log('Wilma updated');
});

const express = require('express');
let app = express();

app.get('/', (req,res) => {   
    res.send('Mongoose created: '+newUser);  
});

const server = app.listen(3000, () => {  
    const SERVERHOST = server.address().address;      
    const SERVERPORT = server.address().port;  
    console.log('App listening at http://${SERVERHOST}:${SERVERPORT}');
});
