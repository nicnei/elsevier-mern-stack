var express = require("express");
var app = express();

var router = express.Router();
app.use("/cinema",router);

router.get("/",(req,res)=>{
  res.json({"message" : "Hello Elsevier"});
});

app.listen(3000,()=>{
  console.log("Live at Port 3000");
});