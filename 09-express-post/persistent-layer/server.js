var mongoose = require('mongoose');
var express = require("express");
const bodyParser = require('body-parser');
var app = express();
var router = express.Router();

mongoose.connect('mongodb://localhost:27017/users',{useNewUrlParser: true}, error => {
	error ? console.log('Connection failed: ${error}') :
	console.log('Successfully connected to MongoDB');
});

var userSchema = new mongoose.Schema(
    {
      _id:        { type: String, required: true }, 
      first_name: { type: String, required: true },
      last_name:  { type: String, required: true },
      email:      { type: String, required: true }
    }
);
var User = mongoose.model('User', userSchema);
router.use(bodyParser.json());
app.use("/cinema",router);
app.use("/cinema/add",router);

router.route('/').get((req,resp)=>{
	console.log("GET");
	var query = User.find({},'_id first_name last_name email').then(users => {
		if (!users) {
			resp.json({message: 'No users'});
			return;
		} else {
			resp.json(users);
			return;
		}
	});
});

router.route('/').post((req,resp)=>{
	console.log("POST: "+JSON.stringify(req.body));
	User.create(req.body, (error,result) => {
		if (error) console.log('Failed to create: '+error);
		else console.log('Successfully added user');
	});
});

router.route('/:id').get((req,resp)=>{
	console.log("GET ID");
	const id = req.params.id;
	var query = User.findOne({'_id': id}).then(user => {
		if (!user) {
			resp.json({message: 'User not found'});
			return;
		} else {
			resp.json({user});
			return;
		}
	});
});

app.listen(5000,()=>{
  console.log("Live at Port 5000");
});